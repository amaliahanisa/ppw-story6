from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from .forms import StatusForm
from .models import Status
from .views import create_status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UrlTest(TestCase):
    def test_valid_url(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

class HomepageTest(TestCase):
    def test_hompage_using_create_func(self):
        found = resolve('/')
        self.assertEqual(found.func, create_status)

    def test_homepage_contains_greeting(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Halo, Apa Kabar?", response_content)

    def test_story_post_error_and_render_the_result(self):
        test = ''
        response_post = Client().post('', {'title': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('')
        html_response = response.content.decode('utf-8')
        self.assertIn(test, html_response)

class StatusFormTest(TestCase):
    def test_valid_input(self):
        status_form = StatusForm({'title': "Status"})
        self.assertTrue(status_form.is_valid())
        status = Status()
        status.title = status_form.cleaned_data['title']
        status.save()
        self.assertEqual(status.title, "Status")

    def test_invalid_input(self):
        status_form = StatusForm({
        'description': 301 * "X"
        })
        self.assertFalse(status_form.is_valid())

    def test_single_entry(self):
        Status.objects.create(title='status')
        response = self.client.get('/')
        self.assertContains(response, 'status')
        self.assertEqual(Status.objects.all().count(), 1)

    def test_two_entries(self):
        Status.objects.create(title='status satu')
        Status.objects.create(title='status dua')
        response = self.client.get('/')
        self.assertContains(response, 'status satu')
        self.assertContains(response, 'status dua')
        self.assertEqual(Status.objects.all().count(), 2)

class StatusModelTest(TestCase):
     def test_string_representation(self):
        status = Status(title="Status saya")
        self.assertEqual(str(status), status.title)

class SeleniumTest(LiveServerTestCase):
    def setUp(self):
        chrome_op = Options()
        chrome_op.add_argument('--dns-prefetch-disable'),
        chrome_op.add_argument('--no-sandbox'),
        chrome_op.add_argument('--headless'),
        chrome_op.add_argument('disable-gpu'),
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_op)
        super(SeleniumTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTest, self).tearDown()

    def test_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        new_status = selenium.find_element_by_id('id_title')
        new_status.send_keys("tes status")
        time.sleep(5)
        submit = selenium.find_element_by_class_name('submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(10)
        selenium.get(self.live_server_url)
        time.sleep(5)
        self.assertIn('tes status', selenium.page_source)
