from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def create_status(request):
    if request.method == "POST":
        form = StatusForm(request.POST or None)
        if form.is_valid():
            status = Status()
            status.title = form.cleaned_data['title']
            status.save()
        return redirect('/')
    else:
        form = StatusForm()
        status = Status.objects.all()
        response = {'status': status, 'title': 'List', 'form':form}
        return render(request, "homepage.html", response)